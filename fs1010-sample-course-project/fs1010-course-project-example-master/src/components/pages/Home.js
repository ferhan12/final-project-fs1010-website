import React from "react";
import {
  Container,
  Row,
  Col,
  Button,
  CardBody,
  CardTitle,
  CardText,
  CardFooter,
  Card,
} from "reactstrap";
import mepic from "./pics/me.jpg";

import image from "./pics/d.jpg";

const Home = () => {
  return (
    <Container>
      <Row className="my-5">
        <Col lg="7">
          <img
            className="img-fluid rounded mb-4 mb-lg-0"
            img
            src={mepic}
            alt="Ferhan"
          />

          <img
            className="img-fluid rounded mb-4 mb-lg-0"
            img
            src={image}
            alt="coding"
          />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">Ferhan Qamar </h1>
          <p>
            Hey! welcome to my site! My name is Ferhan. I have a passion for
            coding and web development.{" "}
          </p>
          <p>
            Come follow me on my journey thorough my career and passion! If i am
            not coding, I am in the gym or on the basketball court!
          </p>
        </Col>
      </Row>
      <Card className="bg">
        <CardBody>
          <CardText>
            <p7 className="text">
              Life begins at the end of your comfort zone! Just a web developer
              exploring the online world and leaving my mark. Be sure to follow
              and check out my work!{" "}


              
            </p7>
          </CardText>
        </CardBody>
      </Card>
      <Row></Row>
    </Container>
  );
};

export default Home;
