import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import pic from "./pics/ph.jpg";

import codepic from "./pics/code.jpg";

const About = () => {
  return (
    <Container>
      <Row className="my-5">
        <Col lg="7">
          <img
            className="img-fluid rounded mb-4 mb-lg-0"
            img
            src={pic}
            alt="Logo"
          />

          <img
            className="img-fluid rounded mb-4 mb-lg-0"
            img
            src={codepic}
            alt="codepic"
          />
        </Col>
        <Col lg="5">
          <h1 className="font-weight-light">About Me</h1>
          <p>
            I am new to web development and programming. A fast and avid
            learner. </p>
            
            <p> Projects I have completed:
             <li> <a href="https://gitlab.com/ferhan12/to-do-list-ferhan/-/tree/to-do">To Do list</a> </li>
            <li> <a href=" https://gitlab.com/ferhan12/assignment-1-style-photo-gallery-ferhan-qamar ">Photo gallery</a> </li> </p>
                

           
            <p>Curently, I am wokring on a few projects
            with some start up companies; through Venture for Canada. Stay tuned.
         

         </p>

          <p>
            Education: <li>High School Diploma- Honours</li>
            <li>Seneca College- Hospitality Management</li>
            <li>York University- Full Stack Web Develeopment</li>
          </p>

          <Button color="primary" href="/contact">
            Contact Me
          </Button>

          <Button color="primary" href="./ferhan-ahmad.pdf" download>
            View My Resume
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default About;
