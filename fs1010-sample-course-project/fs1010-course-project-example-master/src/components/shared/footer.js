import React from "react";
import { Container } from "reactstrap";
import { SocialIcon } from "react-social-icons";

const Footer = () => {
  return (
    <footer className="py-5">
      <Container>
        <p className="m-0 text-center text-white">
          Coded by Ferhan Qamar- Copyright 2021{" "}
          <SocialIcon
            url="https://email:ferhan.qamar@gmail.com/"
            bgColor="#ff5a01"
          />{" "}
          <SocialIcon url="https://ca.linkedin.com/" bgColor="#ff5a01" />{" "}
          <SocialIcon url="https://gitlab.com/ferhan12" bgColor="#ff5a01" />

         
        </p>
      </Container>
    </footer>
  );
};

export default Footer;


