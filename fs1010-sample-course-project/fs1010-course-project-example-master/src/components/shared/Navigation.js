import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import { SocialIcon } from 'react-social-icons';




const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    return (
        <Navbar dark  expand="md" fixed="top">
            <Container>
                <NavbarBrand href="">Ferhan Qamar 

                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouteLink} to="/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouteLink} to="/about">About Me</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouteLink} to="/contact">Contact Me</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouteLink} to="/submissions">Submissions</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Container>
        </Navbar>
    )
}

export default Navigation